public class END {
    public static void main(String[] args) {
        Figure qdrt = new Figure();
        qdrt.corners = 4;
        qdrt.sidesize1 = 5;
        qdrt.sidesize2 = 6;
        qdrt.name = "Квадрат";
        qdrt.S = 5 * 6;
        qdrt.displayInfo();

        Figure ormg = new Figure();
        qdrt.corners = 4;
        qdrt.sidesize1 = 5;
        qdrt.sidesize2 = 10;
        qdrt.name = "Прямоугольник";
        qdrt.S = 5 * 10;
        qdrt.displayInfo();

        Figure trangl = new Figure();
        qdrt.corners = 3;
        qdrt.sidesize1 = 4;
        qdrt.sidesize2 = 5;
        qdrt.sidesize3 = 6;
        qdrt.name = "Треугольник";
        qdrt.S = 6;
        qdrt.displayInfo();


    }
}

class Figure{
    String name;
    int sidesize1;
    int sidesize2;
    int sidesize3;
    int corners;
    int S;
    void displayInfo(){
        System.out.println("Название "+ name + " Сторона 1 " + sidesize1 + " Сторона 2 " + sidesize2 + " Сторона 3 " + sidesize3 + " Количество углов " + corners + " Площадь фигуры " + S );
    }

}